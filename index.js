// console.log("Hello World!");
// s33 Avtivity - Postman and REST API

/*
	Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

	Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

*/

	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET'
	})
	.then((response) => response.json()) 
	.then((json) => {

		const todoTitles = json.map(todo => todo.title);

		console.log(todoTitles);

	})

/*
	Create a fetch request using the GET method that will retrieve a
	single to do list item from JSON Placeholder API.

	Using the data retrieved, print a message in the console that will
	provide the title and status of the to do list item.

*/

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'GET'		
	})
	.then((response) => response.json()) 
	.then((json) => console.log(json));

/*
	Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.	

*/
	fetch('https://jsonplaceholder.typicode.com/todos/', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
			userId: 1
		})
	})
	.then((response) => response.json())	
	.then((json) => console.log(json));

/*
	Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

	Update a to do list item by changing the data structure to contain
	the following properties:
		a. Title
		b. Description
		c. Status
		d. Date Completed
		e. User ID

*/
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Update To Do List Item',
			description: "To update the my to do list with a different strucure",
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
	})
	.then((response) => response.json()) 
	.then((json) => console.log(json));

/*
	Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

	Update a to do list item by changing the status to complete and add a date when the status was changed.

*/
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "03/30/2023",
			userId: 1
		})
	})
	.then((response) => response.json()) 
	.then((json) => console.log(json));

/*
	Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

*/
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'DELETE'
	});

